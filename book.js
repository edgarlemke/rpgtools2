const rule_content = document.getElementById('rule-content')
rule_content.innerHTML = Rule.get_html()

const species_content = document.getElementById('species-content')
species_content.innerHTML = Species.get_html()

const class_content = document.getElementById('class-content')
class_content.innerHTML = Class.get_html()

const weapon_content = document.getElementById('weapon-content')
weapon_content.innerHTML = Weapon.get_html()

const defense_content = document.getElementById('defense-content')
defense_content.innerHTML = Defense.get_html()

const ammo_content = document.getElementById('ammo-content')
ammo_content.innerHTML = Ammo.get_html()

const generic_content = document.getElementById('generic-content')
generic_content.innerHTML = Generic.get_html()

const drug_content = document.getElementById('drug-content')
drug_content.innerHTML = Drug.get_html()

const attack_content = document.getElementById('attack-content')
attack_content.innerHTML = Attack.get_html()

const trick_content = document.getElementById('trick-content')
trick_content.innerHTML = Trick.get_html()
